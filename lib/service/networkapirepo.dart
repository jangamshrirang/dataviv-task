import 'package:task_app/signup.dart';

import 'package:http/http.dart' as http;

import 'dart:convert';

class API_Manager {
  Future signUp(String firstName, String userEmail, String userPhone,
      String userPass) async {
    var url = 'https://mmapi.datavivservers.in/customer/signup';
    var response = await http.post(url, body: {
      'firstName': '$firstName',
      'mobilePhone': '$userPhone',
      'emailAddress': '$userEmail',
      'password': '$userPass'
    });
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    var convertDatatoJson = jsonDecode(response.body);
    return convertDatatoJson;
  }

  Future login(String userEmail, String userpass) async {
    var url = 'https://mmapi.datavivservers.in/customer/login';
    var response = await http.post(url, body: {
      'user': '$userEmail',
      'password': '$userpass',
    });
    var convertDatatoJson = jsonDecode(response.body);
    return convertDatatoJson;
  }

  Future otpVerify(String userEmail, var otp) async {
    var url = 'https://mmapi.datavivservers.in/customer/verifyotp';
    var intotp = int.parse(otp, onError: (source) => -1);
    var response = await http.post(url, body: {
      'emailAddress': '$userEmail',
      'otp': '$intotp',
    });
    var convertDatatoJson = jsonDecode(response.body);
    return convertDatatoJson;
  }

  Future forgetPass(
    String userEmail,
  ) async {
    var url = 'https://mmapi.datavivservers.in/customer/forgotpassword';
    var response = await http.post(url, body: {
      'emailAddress': '$userEmail',
    });
    var convertDatatoJson = jsonDecode(response.body);
    return convertDatatoJson;
  }

  Future resetPass(
    String token,
    String userPass,
  ) async {
    var url =
        'https://mmapi.datavivservers.in/customer/forgotpassword-reset/$token';
    var response = await http.post(url, body: {
      'password': '$userPass',
    });
    var convertDatatoJson = jsonDecode(response.body);
    return convertDatatoJson;
  }
}
