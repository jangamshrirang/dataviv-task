import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../service/networkapirepo.dart';

class ForgetPasswordScreen extends StatefulWidget {
  @override
  _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  TextEditingController userEmail = TextEditingController();
  TextEditingController userPass = TextEditingController();
  String message = '';
  String token = '';
  String message2 = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Forget Password'),
        backgroundColor: Color.fromRGBO(31, 125, 122, 1),
      ),
      body: Container(
        child: Form(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 0.1.sh,
              ),
              textField(
                  hint: "Email ID",
                  icon: Icons.email,
                  textcontroller: userEmail),
              SizedBox(
                height: 0.01.sh,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Material(
                  elevation: 2.0,
                  clipBehavior: Clip.hardEdge,
                  borderRadius: BorderRadius.circular(10),
                  color: Color.fromRGBO(31, 125, 122, 1),
                  child: InkWell(
                    onTap: () async {
                      var res = await API_Manager().forgetPass(
                        userEmail.text,
                      );
                      setState(() {
                        message = res['message'];
                        token = res['d']['resetpasswordtoken'];
                      });
                    },
                    child: Container(
                        padding: EdgeInsets.all(9.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: Text(
                          'Forget Password',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        )),
                  ),
                ),
              ),
              SizedBox(
                height: 0.03.sw,
              ),
              Container(
                  width: 0.5.sw,
                  child: Text(
                    message,
                    softWrap: true,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  )),
              SizedBox(
                height: 0.03.sw,
              ),
              textField(
                  hint: "Password",
                  icon: Icons.security,
                  textcontroller: userPass),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Material(
                  elevation: 2.0,
                  clipBehavior: Clip.hardEdge,
                  borderRadius: BorderRadius.circular(10),
                  color: Color.fromRGBO(31, 125, 122, 1),
                  child: InkWell(
                    onTap: () async {
                      print(token);
                      var res =
                          await API_Manager().resetPass(token, userPass.text);

                      setState(() {
                        message2 = res['message'];
                      });
                    },
                    child: Container(
                        padding: EdgeInsets.all(9.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: Text(
                          'Reset Password',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        )),
                  ),
                ),
              ),
              SizedBox(
                height: 0.03.sw,
              ),
              Container(
                  width: 0.5.sw,
                  child: Text(
                    message2,
                    softWrap: true,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  )),
              SizedBox(
                height: 0.03.sw,
              ),
            ],
          ),
        ),
      ),
    );
  }


  textField(
      {String hint, IconData icon, TextEditingController textcontroller}) {
    return Center(
      child: Container(
        width: 0.6.sw,
        decoration: BoxDecoration(),
        child: TextFormField(
          controller: textcontroller,
          cursorColor: Colors.black,
          // keyboardType: TextInputType.,
          decoration: InputDecoration(
            hintStyle: TextStyle(fontSize: 17),
            hintText: hint,
            // prefix: Icon(Icons.mail)
            prefixIcon: Icon(
              icon,
              color: Color.fromRGBO(31, 125, 122, 1),
            ),
            // suffixIcon: Icon(Icons.search),
            // border: InputBorder.none,
            contentPadding: EdgeInsets.all(18),
          ),
        ),
      ),
    );
  }
}
