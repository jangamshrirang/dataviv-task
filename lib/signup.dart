import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:rive/rive.dart';
import 'package:flutter/services.dart';
// import 'package:flare_flutter/flare_actor.dart';
import './service/networkapirepo.dart';
import 'loginpage.dart';
import 'package:http/http.dart' as http;

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final riveFileName = 'assets/new_file.riv';
  Artboard _artboard;
  TextEditingController userName = TextEditingController();
  TextEditingController userPass = TextEditingController();
  TextEditingController userPhone = TextEditingController();
  TextEditingController userEmail = TextEditingController();
  String message = '';
  String googleimg =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png';
  String fbimg =
      'https://bestforandroid.com/apk/wp-content/uploads/2020/05/facebook-lite-apk-featured-image.png';
  // NetworkapiClient _networkapiClient = NetworkapiClient();
  // NetWorkAPiRepository _netWorkAPiRepository = NetWorkAPiRepository();
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  // var url = "http://example.com/whatsit/create";

// http.post(url, body: {"name": "doodle", "color": "blue"})
//     .then((response) {
//   print("Response status: ${response.statusCode}");
//   print("Response body: ${response.body}");
// });

  @override
  void initState() {
    _loadRiveFile();
    super.initState();
  }

  // loads a Rive file
  void _loadRiveFile() async {
    final bytes = await rootBundle.load(riveFileName);
    final file = RiveFile();

    if (file.import(bytes)) {
      // Select an animation by its name
      setState(() => _artboard = file.mainArtboard
        ..addController(
          SimpleAnimation('idle'),
        ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 0.25.sh,
                  child: Row(
                    children: [
                      Column(
                        children: [
                          SizedBox(
                            height: 0.05.sh,
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => LoginPage(),
                              ));
                            },
                            child: Icon(
                              Icons.arrow_back_ios,
                              size: 30,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SizedBox(
                              height: 0.03.sh,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Text(
                              'Create',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Text(
                              ' Account ',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        height: 200,
                        // width: double.infinity,
                        width: 0.67.sw,
                        child: _artboard != null
                            ? Rive(
                                artboard: _artboard,
                                fit: BoxFit.cover,
                              )
                            : Container(),
                      )
                    ],
                  ),
                ),
                textField(
                    hint: 'Enter FirstName',
                    icon: Icons.person,
                    textcontroller: userName),
                textField(
                    hint: 'Enter Email Id',
                    icon: Icons.mail,
                    textcontroller: userEmail),
                textField(
                    hint: 'Enter Phone No',
                    icon: Icons.phone,
                    textcontroller: userPhone),
                textField(hint: 'Password', textcontroller: userPass),
                // textField(
                //   hint: 'Confirm Password',textcontroller: user
                // ),
                SizedBox(
                  height: 0.05.sw,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('By continuing, you agree to Our '),
                    Text('Terms of',
                        style: TextStyle(fontWeight: FontWeight.bold))
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('service',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    Text('and'),
                    Text('Privacy Policy',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(
                  height: 0.03.sw,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        ' Sign Up ',
                        style: TextStyle(fontSize: 19),
                      ),
                    ),
                    SizedBox(
                      width: 0.3.sw,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Material(
                        elevation: 2.0,
                        clipBehavior: Clip.hardEdge,
                        borderRadius: BorderRadius.circular(50),
                        color: Color.fromRGBO(31, 125, 122, 1),
                        child: InkWell(
                          onTap: () async {
                            // print(userEmail.text);
                            // print(userPass.text);
                            // print(userPhone.text);
                            // print(userName.text);
                            var response = await API_Manager().signUp(
                                userName.text,
                                userEmail.text,
                                userPhone.text,
                                userPass.text);
                            setState(() {
                              message = response['message'];
                            });
                          ;
                          },
                          child: Container(
                            padding: EdgeInsets.all(9.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              
                            ),
                            child: Icon(
                              Icons.arrow_forward_ios,
                              size: 22,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 0.03.sw,
                ),
                Container(
                    width: 0.5.sw,
                    child: Text(
                      message,
                      softWrap: true,
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    )),
                SizedBox(
                  height: 0.03.sw,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Divider(
                        thickness: 1,
                        color: Color(0xff818181),
                      ),
                    ),
                    SizedBox(width: 10),
                    Text(
                      'or Login With',
                      style: TextStyle(
                          // color: Color(0xff818181),
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Divider(
                        thickness: 1,
                        color: Color(0xff818181),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 0.03.sh,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [circleButton(googleimg), circleButton(fbimg)],
                ),
                SizedBox(
                  height: 0.02.sh,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Already  have an account ?'),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => LoginPage(),
                        ));
                      },
                      child: Text('Sign In'),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  circleButton(
    String img,
  ) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        elevation: 2.0,
        clipBehavior: Clip.hardEdge,
        borderRadius: BorderRadius.circular(50),
        color: Colors.white,
        child: InkWell(
          onTap: () {
            // API_Manager().login(userEmail, userPass);
          },
          child: Container(
              padding: EdgeInsets.all(9.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                // border: Border.all(color: Colors.blue, width: 1.4)
              ),
              child: Image.network(img, height: 50, width: 50)),
        ),
      ),
    );
  }

  textField(
      {String hint, IconData icon, TextEditingController textcontroller}) {
    return Center(
      child: Container(
        width: 0.6.sw,
        decoration: BoxDecoration(),
        child: TextFormField(
          controller: textcontroller,
          cursorColor: Colors.black,
          // keyboardType: TextInputType.,
          decoration: InputDecoration(
            hintStyle: TextStyle(fontSize: 17),
            hintText: hint,
            // prefix: Icon(Icons.mail)
            prefixIcon: Icon(
              icon,
              color: Color.fromRGBO(31, 125, 122, 1),
            ),
            // suffixIcon: Icon(Icons.search),
            // border: InputBorder.none,
            contentPadding: EdgeInsets.all(18),
          ),
        ),
      ),
    );
  }
}
